<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(!Auth::check()) {
        return redirect('/login');
    }
    else{
        return redirect('/home');
    }
});

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::get('/home', 'HomeController@index');
    Route::get('/profile', 'HomeController@profile');
    Route::post('/profile', 'HomeController@saveprofile');
    Route::resource('/admin/users', 'Admin\UserController');
    Route::resource('/roles', 'Admin\RoleController');
    Route::resource('admin/posts', 'Admin\\PostsController');
});
