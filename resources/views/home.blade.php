@extends('admin.layouts.master')
@section('content')
    <style>
        .dividerow {
            display: flex;
        }

        .dividerow > div {
            flex: 1;
            background: white;
            border: 2px solid grey;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-10 ">
                <div class="dividerow">
                    <div class="panel panel-success">
                        {!! Charts::styles() !!}
                        <div class="panel-heading">Chart Diagram of {{$name}}</div>

                        <div class="panel-body ">
                            <a href="{{ url('/') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <h4> Actor Name: <span style='color:green'>{{$name}}</span></h4>
                            <span class="chartdiagram">{!! $chart->html() !!}</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="panel panel-default dividerow col-md-10">
            <div class="panel-body">
                <span class="chartdiagram">{!! $prediction->html() !!}</span>
            </div>
        </div>
    </div>
    {!! Charts::scripts() !!}
    {!! $chart->script() !!}
    {!! $prediction->script() !!}
@endsection