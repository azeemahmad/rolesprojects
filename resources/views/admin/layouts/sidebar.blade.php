<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                @if(Auth::guard('web')->user()->image)
                    <img src="{{asset('images/profile_image'.'/'.Auth::guard('web')->user()->image)}}"
                         class="img-circle"
                         alt="User Image"/>
                @else
                    <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle"
                         alt="User Image"/>
                @endif
            </div>
            <div class="pull-left info">
                <p>{{ ucwords(Auth::guard('web')->user()->name)}}</p>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Admin-Panel</li>
            <li><a href="{{'/home'}}"><span><i class="fa fa-home"></i> DashBoard</span></a></li>
            @can('view_users')
            <li><a href="{{'/admin/users'}}"><span><i class="fa fa-users"></i> Users</span></a></li>
            @endcan
            @can('view_roles')
            <li><a href="{{'/roles'}}"><span><i class="fa fa-book"></i> Role</span></a></li>
            @endcan
            @can('view_posts')
            <li><a href="{{'/admin/posts'}}"><span><i class="fa fa-envelope"></i> Posts</span></a></li>
            @endcan
            {{--<li class="treeview">--}}
                {{--<a href="#"><span><i class="fa fa-flag"></i> Home Page</span> <i--}}
                            {{--class="fa fa-angle-left pull-right"></i></a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="#"><i class="fa fa-bookmark"></i> About Role Based</a>--}}
                    {{--</li>--}}
                    {{--<li class="treeview">--}}
                        {{--<a href="#"><span><i class="fa fa-plus-circle"></i> Role Based</span> <i--}}
                                    {{--class="fa fa-angle-left pull-right"></i></a>--}}
                        {{--<ul class="treeview-menu">--}}
                            {{--<li><a href="#"><i class="fa fa-comment-o"></i> Description</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#"><i class="fa fa-film"></i> Role Based</a></li>--}}
                        {{--</ul>--}}

                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </section>
</aside>